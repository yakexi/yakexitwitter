<?php
require_once('../config.php');
require_once(ABSPATH.'include/objects/class.user.php');
if($_POST){
	if(isset($_POST['user'])){
		$user = $_POST['user'];
	}else{
		exit;
	}
	$user = new User();
	
}
?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />
<title>Sign in for Yakexi</title>
<link href="../css/base.css" media="screen" rel="stylesheet"	type="text/css" />
<script type='text/javascript'>
  //<![CDATA[
  //]]>
</script>
</head>
<body>
<div id="body">
<div class="container">
<div id="sidebar">

</div>
<div id="content">
<div class="notice">
<p class="confirm hidden"></p>
<p class="error hidden"></p>
</div>
<h1>Sign in!</h1>
<form method="post" id="new_user" class="form-global" action="<?php echo $_SERVER['PHP_SELF'] ?>">
<div style="margin: 0pt; padding: 0pt; display: inline;">
<input type="hidden" value="Xt61l0kPEn7p433QJPEIwKB1j+49DnUfgupMlu4wjOc=" name="authenticity_token">
</div>

<div class="section">
<p>
<label for="user_login">Username</label>
<input type="text" size="30" name="user[login]" id="user_login" class="little">
</p>

<p>
<label for="user_password">Password</label>
<input type="password" size="30" name="user[password]" id="user_password">
</p>

</div>
<input type="submit" value="Sign in" name="commit" id="user_submit" class="button">
</p>
</form>

</div>
</div>
</div>
</body>
</html>