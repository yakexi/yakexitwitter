<?php

?>
<html lang='en' xml:lang='en' xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />
<title>Sign up for Yakexi</title>
<script type='text/javascript'>
  //<![CDATA[
  //]]>
</script>
</head>
<body>
<div id="body">
<div class="container">
<div id="sidebar">

</div>
<div id="content">
<div class="notice">
<p class="confirm hidden"></p>
<p class="error hidden"></p>
</div>
<h1>Sign up!</h1>
<form method="post" id="new_user" class="form-global" action="/users"><div style="margin: 0pt; padding: 0pt; display: inline;"><input type="hidden" value="Xt61l0kPEn7p433QJPEIwKB1j+49DnUfgupMlu4wjOc=" name="authenticity_token"></div>
<div class="section">
<p>
<label for="user_name">Full name</label>
<input type="text" size="30" name="user[name]" id="user_name">
</p>

<p>
<label for="user_email">Email</label>
<input type="text" value="" size="30" name="user[email]" id="user_email">
</p>

</div>
<div class="section">
<p>
<label for="user_login">Username</label>
<input type="text" size="30" name="user[login]" id="user_login" class="little">
</p>

<p>
<label for="user_password">Password</label>
<input type="password" size="30" name="user[password]" id="user_password">
</p>

</div>
<input type="submit" value="Sign up" name="commit" id="user_submit" class="button">
</p>
</form>

</div>
</div>
</div>
</body>
</html>