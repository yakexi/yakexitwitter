<?php
if($_POST){
	header('Content-type: text/plain');
	require_once('../config.php');
	require_once(ABSPATH.'oauth/twitteroauth.php');
	require_once(ABSPATH.'account/security.php');
	require_once(ABSPATH.'include/class.token.php');
	require_once(ABSPATH.'include/class.post.php');
	$sina = '';

	$api_type = isset($_POST['api_type'])?$_POST['api_type']:'';
	$id = isset($_POST['id'])?$_POST['id']:'';

	$token = $_SESSION['access_token'];
	$access_token = $_SESSION['access_token'];
	$post = new Post();
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

	if($id){
		switch($api_type){
			case 'fav':
				$post->twitter = $connection->post("favorites/create/{$id}");
				break;
			default:
				break;
		}
	}

	echo json_encode($post);
}else{
	exit(0);
}