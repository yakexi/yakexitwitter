<?php
if($_POST){
	header('Content-type: text/json');
	$type = $_POST['type'];
	$status = $_POST['status'];
	if(isset($_POST['max_id']))
		$max_id = $_POST['max_id'];
	if(isset($_POST['since_id']))
		$since_id = $_POST['since_id'];
	if(isset($_POST['page']))
		$page = $_POST['page'];
	require_once('../config.php');
	require_once(ABSPATH.'oauth/twitteroauth.php');
	require_once(ABSPATH.'account/security.php');
	require_once(ABSPATH.'include/class.token.php');
	require_once(ABSPATH.'include/render_help.php');
	$token = $_SESSION['access_token'];
	$access_token = $_SESSION['access_token'];	
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
	
	if(isset($max_id)){
		$para = array('max_id' => $max_id-1);
	}else if(isset($since_id)){
		$para = array('since_id' => $since_id);
	}
	
	ob_start();	
	switch($status){
		case 'home_timeline':
			if(isset($para)){
				$content = $connection->get('statuses/home_timeline',$para);
			}else{
				$content = $connection->get('statuses/home_timeline');
			}	
			//$content = unserialize(file_get_contents('../content.txt'));
			render_status($content);
			$timeline = ob_get_contents();
			if(isset($max_id)){//get old
				$data = array(
					"num"=>count($content),
					"max_id"=>count($content)>0?$content[count($content)-1]->id:0,  			
					"timeline"=>$timeline
				);
			}else if(isset($since_id)){//get new
				$data = array(
				    "num"=>count($content),
					"since_id"=>count($content)>0?$content[0]->id:0,  			
					"timeline"=>$timeline
				);
			}else{//get statuses
				$data = array(
					"num"=>count($content),
					"max_id"=>count($content)>0?$content[count($content)-1]->id:0,  			
					"timeline"=>$timeline
				);
			}
			break;
		case 'mentions':
			if(isset($para)){
				$content = $connection->get('statuses/mentions',$para);
			}else{
				$content = $connection->get('statuses/mentions');
			}
			//$content = unserialize(file_get_contents('../content.txt'));
			render_status($content);
			$timeline = ob_get_contents();
			$data = array(
			    "num"=>count($content),
				"max_id"=>count($content)>0?$content[count($content)-1]->id:0,
				"timeline"=>$timeline
			);
			break;
		case 'dmsg'://direct message
			if(isset($para)){
				$content = $connection->get('direct_messages',$para);
			}else{
				$content = $connection->get('direct_messages');
			}
			//file_put_contents('dmsg.txt',serialize($content));
			//$content = unserialize(file_get_contents('../content.txt'));
			render_dmsg($content);
			$timeline = ob_get_contents();
			$data = array(
			    "num"=>count($content),
				"max_id"=>count($content)>0?$content[count($content)-1]->id:0,
				"timeline"=>$timeline
			);
			break;
		case 'fav'://favorites
			if(isset($para)){
				$content = $connection->get('favorites',$para);
			}else{
				$content = $connection->get('favorites');
			}
			render_status($content);
			$timeline = ob_get_contents();
			$data = array(
			    "num"=>count($content),
				"max_id"=>count($content)>0?$content[count($content)-1]->id:0,
				"timeline"=>$timeline
			);
			break;
		default :
			break;
	}
	ob_end_clean();	
	echo json_encode($data);
}else{
	exit(0);
}