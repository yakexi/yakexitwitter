<?php
if($_POST){
	header('Content-type: text/plain');	
	require_once('../config.php');
	require_once(ABSPATH.'oauth/twitteroauth.php');
	require_once(ABSPATH.'oauth/sinaoauth.php');
	require_once(ABSPATH.'account/security.php');
	require_once(ABSPATH.'include/class.token.php');
	require_once(ABSPATH.'include/class.post.php');
	$sina = '';	
	
	$type = $_POST['type'];
	$text = $_POST['text'];
	$data = array();
	$data['status'] = $text;
	if($_POST['sina'])
		$sina = $_POST['sina'];
	if($_POST['in_reply_to_status_id']){
		$data['in_reply_to_status_id'] = $_POST['in_reply_to_status_id'];	
	}

	$token = $_SESSION['access_token'];
	$access_token = $_SESSION['access_token'];
	$post = new Post();
	//post twitter
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
	$post->twitter = $connection->post('statuses/update', $data);	
	//post sina
	if($sina){
		$c = new WeiboClient( WB_AKEY , WB_SKEY , $_SESSION['access_token_sina']['oauth_token'] , $_SESSION['access_token_sina']['oauth_token_secret']  );
		$post->sina = $c->update( $text );
	}
	//var_dump($post);
	echo json_encode($post);
}else{
	exit(0);
}