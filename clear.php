<?php
/**
 * @file
 * Clears PHP sessions and redirects to the connect page.
 */
 
/* Load and clear sessions and saved token*/
session_start();
session_destroy();
if(isset($_REQUEST['type'])){
	switch($_REQUEST['type']){
		case 'twitter':
			unlink('twitter_token');
			break;
		case 'sina':
			unlink('sina_token');
			break;
		case 'all':
			unlink('twitter_token');
			unlink('sina_token');
			break;
		default:
			break;
	}
}
 
/* Redirect to page with the connect to Twitter option. */
header('Location: ./connect.php');
