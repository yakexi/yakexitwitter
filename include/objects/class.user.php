<?php
/*
 This SQL query will create the table to store your object.

 CREATE TABLE `user` (
 `userid` int(11) NOT NULL auto_increment,
 `userid` INT NOT NULL,
 `username` VARCHAR(255) NOT NULL,
 `email` VARCHAR(255) NOT NULL,
 `password` VARCHAR(255) NOT NULL,
 `user_locale` VARCHAR(255) NOT NULL,
 `time_zone` VARCHAR(255) NOT NULL,
 `create_ts` TIMESTAMP NOT NULL,
 `status` TINYINT NOT NULL, PRIMARY KEY  (`userid`)) ENGINE=MyISAM;
 */

/**
 * <b>User</b> class with integrated CRUD methods.
 * @author Php Object Generator
 * @version POG 3.0e / PHP5
 * @copyright Free for personal & commercial use. (Offered under the BSD license)
 * @link http://www.phpobjectgenerator.com/?language=php5&wrapper=pog&objectName=User&attributeList=array+%28%0A++0+%3D%3E+%27userid%27%2C%0A++1+%3D%3E+%27username%27%2C%0A++2+%3D%3E+%27email%27%2C%0A++3+%3D%3E+%27password%27%2C%0A++4+%3D%3E+%27user_locale%27%2C%0A++5+%3D%3E+%27time_zone%27%2C%0A++6+%3D%3E+%27create_ts%27%2C%0A++7+%3D%3E+%27status%27%2C%0A%29&typeList=array+%28%0A++0+%3D%3E+%27INT%27%2C%0A++1+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++2+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++3+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++4+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++5+%3D%3E+%27VARCHAR%28255%29%27%2C%0A++6+%3D%3E+%27TIMESTAMP%27%2C%0A++7+%3D%3E+%27TINYINT%27%2C%0A%29
 */
require_once('class.pog_base.php');
class User extends POG_Base
{
	public $userId = '';

	/**
	 * @var INT
	 */
	public $userid;

	/**
	 * @var VARCHAR(255)
	 */
	public $username;

	/**
	 * @var VARCHAR(255)
	 */
	public $email;

	/**
	 * @var VARCHAR(255)
	 */
	public $password;

	/**
	 * @var VARCHAR(255)
	 */
	public $user_locale;

	/**
	 * @var VARCHAR(255)
	 */
	public $time_zone;

	/**
	 * @var TIMESTAMP
	 */
	public $create_ts;

	/**
	 * @var TINYINT
	 */
	public $status;

	public $pog_attribute_type = array(
		"userId" => array('db_attributes' => array("NUMERIC", "INT")),
		"userid" => array('db_attributes' => array("NUMERIC", "INT")),
		"username" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"email" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"password" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"user_locale" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"time_zone" => array('db_attributes' => array("TEXT", "VARCHAR", "255")),
		"create_ts" => array('db_attributes' => array("NUMERIC", "TIMESTAMP")),
		"status" => array('db_attributes' => array("NUMERIC", "TINYINT")),
	);
	public $pog_query;


	/**
	 * Getter for some private attributes
	 * @return mixed $attribute
	 */
	public function __get($attribute)
	{
		if (isset($this->{"_".$attribute}))
		{
			return $this->{"_".$attribute};
		}
		else
		{
			return false;
		}
	}

	function User($userid='', $username='', $email='', $password='', $user_locale='', $time_zone='', $create_ts='', $status='')
	{
		$this->userid = $userid;
		$this->username = $username;
		$this->email = $email;
		$this->password = $password;
		$this->user_locale = $user_locale;
		$this->time_zone = $time_zone;
		$this->create_ts = $create_ts;
		$this->status = $status;
	}


	/**
	 * Gets object from database
	 * @param integer $userId
	 * @return object $User
	 */
	function Get($userId)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `user` where `userid`='".intval($userId)."' LIMIT 1";
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$this->userId = $row['userid'];
			$this->userid = $this->Unescape($row['userid']);
			$this->username = $this->Unescape($row['username']);
			$this->email = $this->Unescape($row['email']);
			$this->password = $this->Unescape($row['password']);
			$this->user_locale = $this->Unescape($row['user_locale']);
			$this->time_zone = $this->Unescape($row['time_zone']);
			$this->create_ts = $row['create_ts'];
			$this->status = $this->Unescape($row['status']);
		}
		return $this;
	}


	/**
	 * Returns a sorted array of objects that match given conditions
	 * @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...}
	 * @param string $sortBy
	 * @param boolean $ascending
	 * @param int limit
	 * @return array $userList
	 */
	function GetList($fcv_array = array(), $sortBy='', $ascending=true, $limit='')
	{
		$connection = Database::Connect();
		$sqlLimit = ($limit != '' ? "LIMIT $limit" : '');
		$this->pog_query = "select * from `user` ";
		$userList = Array();
		if (sizeof($fcv_array) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$this->pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) != 1)
					{
						$this->pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($fcv_array[$i][2]) ? "BASE64_DECODE(".$fcv_array[$i][2].")" : "'".$fcv_array[$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$fcv_array[$i][0]."`) ".$fcv_array[$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$this->Escape($fcv_array[$i][2])."'";
							$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : "'".$fcv_array[$i][2]."'";
						$this->pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." ".$value;
					}
				}
			}
		}
		if ($sortBy != '')
		{
			if (isset($this->pog_attribute_type[$sortBy]['db_attributes']) && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$sortBy]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$sortBy = "BASE64_DECODE($sortBy) ";
				}
				else
				{
					$sortBy = "$sortBy ";
				}
			}
			else
			{
				$sortBy = "$sortBy ";
			}
		}
		else
		{
			$sortBy = "userid";
		}
		$this->pog_query .= " order by ".$sortBy." ".($ascending ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this);
		$cursor = Database::Reader($this->pog_query, $connection);
		while ($row = Database::Read($cursor))
		{
			$user = new $thisObjectName();
			$user->userId = $row['userid'];
			$user->userid = $this->Unescape($row['userid']);
			$user->username = $this->Unescape($row['username']);
			$user->email = $this->Unescape($row['email']);
			$user->password = $this->Unescape($row['password']);
			$user->user_locale = $this->Unescape($row['user_locale']);
			$user->time_zone = $this->Unescape($row['time_zone']);
			$user->create_ts = $row['create_ts'];
			$user->status = $this->Unescape($row['status']);
			$userList[] = $user;
		}
		return $userList;
	}


	/**
	 * Saves the object to the database
	 * @return integer $userId
	 */
	function Save()
	{
		$connection = Database::Connect();
		$this->pog_query = "select `userid` from `user` where `userid`='".$this->userId."' LIMIT 1";
		$rows = Database::Query($this->pog_query, $connection);
		if ($rows > 0)
		{
			$this->pog_query = "update `user` set
			`userid`='".$this->Escape($this->userid)."', 
			`username`='".$this->Escape($this->username)."', 
			`email`='".$this->Escape($this->email)."', 
			`password`='".$this->Escape($this->password)."', 
			`user_locale`='".$this->Escape($this->user_locale)."', 
			`time_zone`='".$this->Escape($this->time_zone)."', 
			`create_ts`='".$this->create_ts."', 
			`status`='".$this->Escape($this->status)."' where `userid`='".$this->userId."'";
		}
		else
		{
			$this->pog_query = "insert into `user` (`userid`, `username`, `email`, `password`, `user_locale`, `time_zone`, `create_ts`, `status` ) values (
			'".$this->Escape($this->userid)."', 
			'".$this->Escape($this->username)."', 
			'".$this->Escape($this->email)."', 
			'".$this->Escape($this->password)."', 
			'".$this->Escape($this->user_locale)."', 
			'".$this->Escape($this->time_zone)."', 
			'".$this->create_ts."', 
			'".$this->Escape($this->status)."' )";
		}
		$insertId = Database::InsertOrUpdate($this->pog_query, $connection);
		if ($this->userId == "")
		{
			$this->userId = $insertId;
		}
		return $this->userId;
	}


	/**
	 * Clones the object and saves it to the database
	 * @return integer $userId
	 */
	function SaveNew()
	{
		$this->userId = '';
		return $this->Save();
	}


	/**
	 * Deletes the object from the database
	 * @return boolean
	 */
	function Delete()
	{
		$connection = Database::Connect();
		$this->pog_query = "delete from `user` where `userid`='".$this->userId."'";
		return Database::NonQuery($this->pog_query, $connection);
	}


	/**
	 * Deletes a list of objects that match given conditions
	 * @param multidimensional array {("field", "comparator", "value"), ("field", "comparator", "value"), ...}
	 * @param bool $deep
	 * @return
	 */
	function DeleteList($fcv_array)
	{
		if (sizeof($fcv_array) > 0)
		{
			$connection = Database::Connect();
			$pog_query = "delete from `user` where ";
			for ($i=0, $c=sizeof($fcv_array); $i<$c; $i++)
			{
				if (sizeof($fcv_array[$i]) == 1)
				{
					$pog_query .= " ".$fcv_array[$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($fcv_array[$i-1]) !== 1)
					{
						$pog_query .= " AND ";
					}
					if (isset($this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes']) && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$fcv_array[$i][0]]['db_attributes'][0] != 'SET')
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$this->Escape($fcv_array[$i][2])."'";
					}
					else
					{
						$pog_query .= "`".$fcv_array[$i][0]."` ".$fcv_array[$i][1]." '".$fcv_array[$i][2]."'";
					}
				}
			}
			return Database::NonQuery($pog_query, $connection);
		}
	}

	/**
	 * @param username password
	 * @return object $User
	 */
	function CheckLogin($name,$pwd)
	{
		$connection = Database::Connect();
		$this->pog_query = "select * from `user` where `username`='".$this->Escape($name)."' AND `password`='".$this->Escape($pwd)."'";
		$cursor = Database::Reader($this->pog_query, $connection);
		if ($row = Database::Read($cursor))	{
			$this->userId = $row['userid'];
			$this->userid = $this->Unescape($row['userid']);
			$this->username = $this->Unescape($row['username']);
			$this->email = $this->Unescape($row['email']);
			$this->password = $this->Unescape($row['password']);
			$this->user_locale = $this->Unescape($row['user_locale']);
			$this->time_zone = $this->Unescape($row['time_zone']);
			$this->create_ts = $row['create_ts'];
			$this->status = $this->Unescape($row['status']);
		}
		return $this;
	}
}
?>