<?php 
function render_status($content){
foreach($content as $status){
	$user = $status->user;
	$is_rt = false;
	if(isset($status->retweeted_status)){
		$is_rt = true;
		$rt = $status->retweeted_status;
		$user = $rt->user;
	}
	$text = htmlspecialchars($is_rt?$rt->text:$status->text);
?>
<li id="status_<?php echo $status->id;?>" class="hentry u-buaixiatian status">
<span  class="thumb vcard author">
	<a class="tweet-url profile-pic url"	href="http://twitter.com/<?php echo $user->screen_name;?>">
		<img width="48" height="48" src="<?php echo $user->profile_image_url;?>" class="photo fn" alt="<?php echo $user->screen_name;?>">
	</a>
</span> 
<span class="status-body">
	<span class="status-content" id="status_content_<?php echo $status->id;?>">
	<?php if($is_rt){
	?>
	 <span title="" class="big-retweet-icon"></span>
	 <strong><a class="tweet-url screen-name" href="http://twitter.com/<?php echo $user->screen_name;?>"><?php echo $user->screen_name;?></a></strong>	
	 <span class="actions">
	 <div><a title="favorite this tweet" class="fav-action non-fav" id="status_star_19259220746">&nbsp;&nbsp;</a></div>
	 </span> 
	 <span class="entry-content"><?php echo text_format($is_rt?$rt->text:$status->text);?></span>
	<?php }else{?>
	 <strong><a class="tweet-url screen-name" href="http://twitter.com/<?php echo $user->screen_name;?>"><?php echo $user->screen_name;?></a></strong>	
	 <span class="actions">
	 <div><a title="<?php echo $status->favorited?'un-':'';?>favorite this tweet" class="fav-action <?php echo $status->favorited?'fav':'non-fav';?>" id="status_star_<?php echo $status->id;?>" 
	 		data="{'id':'<?php echo $status->id;?>','favorited':<?php echo $status->favorited?1:0;?>}">&nbsp;&nbsp;</a></div>
	 </span> 
	 <span class="entry-content"><?php echo text_format($status->text);?></span>
	<?php }?>
	</span> 
	<span data="{}" class="meta entry-meta"> 
		<a href="http://twitter.com/<?php echo $user->screen_name;?>/status/<?php echo $is_rt?$rt->id:$status->id;?>" rel="bookmark" class="entry-date"> 
			<span data="{time:'<?php echo $status->created_at;?>'}" class="published timestamp"><?php echo time_render_help($status->created_at);?></span>
		</a> <span>via <?php echo $is_rt?$rt->source:$status->source;?></span>	
	</span>
	<?php if($is_rt){?>
	<span class="meta entry-meta retweet-meta">
      <span class="shared-content">Retweeted by <a title="10 days ago" data="{time:&quot;Tue Jul 13 03:30:16 +0000 2010&quot;}" class="screen-name timestamp-title" href="/<?php echo $status->user->screen_name;?>"><?php echo $status->user->screen_name;?></a> <!-- and 1 other--></span>
    </span>
    <?php }?>
	<ul class="actions-hover">
		<li><span class="reply"> <span class="reply-icon icon"></span> <a
			title="reply to <?php echo $user->screen_name;?>"
			data="{id:'<?php echo $status->id;?>',in_reply_to:'<?php echo $user->screen_name?>'}"
			href="/?status=<?php echo $text;?>&in_reply_to_status_id=19259220746&in_reply_to=<?php echo $user->screen_name;?>">Reply</a>
		</span></li>	
		<li><span class="retweet-link"> <span class="retweet-icon icon"></span>
		<a href="#" title="Retweet" data="{id:'<?php echo $status->id;?>',text:'<?php echo $user->screen_name.' '.$text;?>'}">Retweet</a> </span></li>
	
	</ul>
	<ul class="meta-data clearfix"></ul>
</span>
</li>
<?php 
	}//end of foreach
	//echo '<pre>';var_dump($content);echo '</pre>';
}//end of function render_status

function render_dmsg($content){
foreach($content as $status){
	$user = $status->sender;	
	$text = htmlspecialchars($status->text);
?>
<li id="status_<?php echo $status->id;?>" class="hentry u-buaixiatian status">
<span  class="thumb vcard author">
	<a class="tweet-url profile-pic url"	href="http://twitter.com/<?php echo $user->screen_name;?>">
		<img width="48" height="48" src="<?php echo $user->profile_image_url;?>" class="photo fn" alt="<?php echo $user->screen_name;?>">
	</a>
</span> 
<span class="status-body">
	<span class="status-content" id="status_content_<?php echo $status->id;?>">
	 <strong><a class="tweet-url screen-name" href="http://twitter.com/<?php echo $user->screen_name;?>"><?php echo $user->screen_name;?></a></strong>	
	 <span class="actions">
	 </span> 
	 <span class="entry-content"><?php echo text_format($status->text);?></span>
	</span> 
	<span data="{}" class="meta entry-meta"> 
		<a href="http://twitter.com/<?php echo $user->screen_name;?>/status/<?php echo $status->id;?>" rel="bookmark" class="entry-date"> 
			<span data="{time:'<?php echo $status->created_at;?>'}" class="published timestamp"><?php echo time_render_help($status->created_at);?></span>
		</a>
	</span>	
	<ul class="actions-hover">
		<li><span class="reply"> <span class="reply-icon icon"></span> <a
			title="reply to <?php echo $user->screen_name;?>"
			data="{id:'<?php echo $status->id;?>',in_reply_to:'<?php echo $user->screen_name?>'}"
			href="/?status=<?php echo $text;?>&in_reply_to_status_id=19259220746&in_reply_to=<?php echo $user->screen_name;?>">Reply</a>
		</span></li>
	</ul>
</span>
</li>
<?php 
	}//end of foreach
	//echo '<pre>';var_dump($content);echo '</pre>';
}//end of function render_status

function text_format($text){
	$text =  preg_replace('/\s*((?:http:\/\/)[^\s]+\.[^\s]+)(?:\s+|$)/i','<a target="_blank" rel="nofollow" class="tweet-url web" href="$1">$1</a> ',$text);
	$text =  preg_replace('/(?<![_\w])@(\w+)(?=[:\s])/i','@<a class="tweet-url screen-name" href="http://twitter.com/$1">$1</a>',$text);	
	return $text;
}

function time_render_help($str){
	return $str;
}