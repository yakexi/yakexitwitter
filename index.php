<?php
/**
 * @file
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.
 */

/* Load required lib files. */

require_once('config.php');
require_once('oauth/twitteroauth.php');
require_once('account/security.php');
require_once('include/class.token.php');
require_once('include/render_help.php');
include_once( 'oauth/sinaoauth.php');

//Token::save('twitter',array('oauth_token'=>'key','oauth_token_secret'=>'sercet'));
/* If access tokens are not available redirect to connect page. */
if($token = Token::get('twitter')){
	$_SESSION['access_token'] = $token;
}else{
	header('Location: ./clearsessions.php');
	exit;
}
if($token_sina = Token::get('sina')){
	$_SESSION['access_token_sina'] = $token_sina;
	$access_token_sina = $_SESSION['access_token_sina'];
}
$username = $_SESSION['login_user']['username'];

/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

$content = unserialize(file_get_contents('content.txt'));
$user_show = unserialize(file_get_contents('user_show_iyakexi.txt'));
//$content = $connection->get('statuses/home_timeline');
//$user_show = $connection->get('users/show', array('screen_name' => $username?$username:'iyakexi'));
//echo "<pre>$username:";var_dump($user_show);echo '</pre>';


/* Some example calls */
//$connection->get('users/show', array('screen_name' => 'abraham')));
//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
//$connection->post('statuses/destroy', array('id' => 5437877770));
//$connection->post('friendships/create', array('id' => 9436992)));
//$connection->post('friendships/destroy', array('id' => 9436992)));

/* Include HTML to display on the page */
include('html.inc.php');