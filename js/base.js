$(function(){
	var y = yakexi;
	$('#tweeting_button').click(y.h.post);
	$('#status').keyup(y.h.postKeyEvent);
	$('#status').keydown(y.h.postKeyEvent);
	$('#more').click(y.h.getOld);
	$('#results_update').click(y.h.updateNew);
	$('#primary_nav a').click(y.h.switchTab);
	$('.retweet-link a').click(y.h.retweet);
	$('.reply a').click(y.h.reply);
	$('.fav-action').click(y.h.favorites.create);
	y.h.intervalId = setInterval(home.getNew,30*1000);
});

var home = {
	maxId:0,
	page:1,
	bGetOld:true,
	intervalId:0,
	newData:{
		num:0,
		text:'',
		sinceId:0
	},
	status:'home_timeline',
	post:function(){
		var text = $('#status').val();
		var chb_sina = $('#chb_sina').attr('checked');
		if(text.replace(/ /gi,'') == '')
			return false;
		var data = {'type':'twitter','text':text};
		var in_reply_to_status_id = $('#in_reply_to_status_id').val();
		if(in_reply_to_status_id) data.in_reply_to_status_id = in_reply_to_status_id;
		if(chb_sina) data.sina = 1;		
		$.ajax({
			type:'POST',
			dataType:'json',
			url:'ajax/post.php',
			data:data,
			success:function(data){
				$('#status').val('');
				$('#in_reply_to_status_id').val('');
				var str = '';
				if(data.twitter.error){
					alert('twitter error:'+data.twitter.error);
				}else{
					str += 'twitter:'+data.twitter.id;
				}
				if(chb_sina){
					if(data.sina.error){
						alert('sina error:'+data.sina.error);
					}else{
						str += '\nsina:'+data.sina.id;
					}
				}
				if(str){
					alert(str);
				}
			},
			error:function(data){
				alert('post error'+data);
				$('#in_reply_to_status_id').val('');
			}
		});
		return false;
	},
	retweet:function(){		
		var d = eval('('+$(this).attr('data')+')');
		var t = 'RT @' + d.text;
		$('#status').val(t);
		$('#tweeting_button').removeClass('btn-disabled');	
		$('#status').focus();
		return false;
	},
	reply:function(){
		var d = eval('('+$(this).attr('data')+')');		
		$('#status').val('@'+d.in_reply_to+' ');
		$('#tweeting_button').removeClass('btn-disabled');
		$('#in_reply_to').val(d.in_reply_to);
		$('#in_reply_to_status_id').val(d.id);
		$('#status').focus();
		return false;
	},
	postKeyEvent:function(){
		var text = $('#status').val();
		if(text.replace(/ /gi,'')!='')
			$('#tweeting_button').removeClass('btn-disabled');
		else
			$('#tweeting_button').addClass('btn-disabled');
	},
	getOld:function(){
		var that = home;
		if(!that.bGetOld) return false;
		that.bGetOld = false;
		$('#timeline li:last-child').addClass('last-on-page');
		$('#more').addClass('loading').html('');		
		$.ajax({
			type:'POST',
			dataType:'json',
			url:'ajax/get_statuses.php',
			data:{'type':'twitter','status':that.status,'max_id':that.maxId,'page':that.page+1},
			success:function(data){
				if(data.num<20)	$('#pagination').hide();
				that.bGetOld = true;
				that.maxId = data.max_id;
				that.page++;
				var obj = $(data.timeline);		
				obj.find('.retweet-link a').click(that.retweet);
				obj.find('.reply a').click(that.reply);
				obj.find('.fav-action').click(that.favorites.create);
				$('#timeline').append(obj);
				$('#more').removeClass('loading').html('more');
			},
			error:function(data){
				that.bGetOld = true;
				$('#more').removeClass('loading').html('more');
			}
		});
		return false;
	},
	getNew:function(){
		var that = home;
		$.ajax({
			type:'POST',
			dataType:'json',
			url:'ajax/get_statuses.php',
			data:{'type':'twitter','status':'home_timeline','since_id':that.newData.sinceId},
			success:function(data){
				if(data.num>0){
					that.newData.num += data.num;
					that.newData.sinceId = data.since_id;
					that.newData.timeline = data.timeline + that.newData.timeline;					
					$('#results_update').html(that.newData.num + ' new tweet'+(that.newData.num>1?'s':'')).parent().show();
					document.title ='(' + that.newData.num + ') 亚克蜥-@iyakexi';
					// alert(that.newData.num+' | '+that.newData.sinceId);
				}
			},
			error:function(data){
				
			}
		});		
	},
	updateNew:function(){
		var that = home;
		$('#new_results_notification').hide();
		var obj = $(that.newData.timeline);
		obj.find('.retweet-link a').click(that.retweet);
		obj.find('.reply a').click(that.reply);
		obj.find('.fav-action').click(that.favorites.create);
		obj.last().addClass('last-on-refresh');
		$('#timeline').prepend(obj);
		document.title = '亚克蜥-@iyakexi';
		that.newData.num = 0;
		that.newData.timeline = '';
		return false;
	},
	clearData:function(){
		var that = home;
		$('#new_results_notification').hide();
		document.title = '亚克蜥-@iyakexi';
		that.newData.num = 0;
		that.newData.timeline = '';
	},
	switchTab:function(){
		var that = home;
		var o = $(this);
		o.parent().parent().children().removeClass('active loading');
		o.parent().addClass('active loading');
		that.clearData();
		switch(o.parent().attr('id')){
			case 'home_tab':
				$('#heading').html('Home');
				that.getStatus(o);
				that.intervalId = setInterval(that.getNew,30*1000);
				break;
			case 'replies_tab':				
				that.getReplies(o);
				break;
			case 'direct_messages_tab':
				that.getDMsg(o);
				clearInterval(that.intervalId);
				break;
			case 'favorites_tab':
				that.getFavorites(o);
				clearInterval(that.intervalId);
				break;
			case 'retweets_by_others_tab':
				clearInterval(that.intervalId);
				break;
			default:
				break;		
		}		
		return false;
	},
	getReplies:function(o){
		var that = this;
		that.status = 'mentions';
		clearInterval(that.intervalId);
		$.ajax({
			type:'POST',
			dataType:'json',
			url:'ajax/get_statuses.php',
			data:{'type':'twitter','status':'mentions'},
			success:function(data){
				$('#heading').html('Mention');
				if(data.num>0){
					if(data.num<20)	
						$('#pagination').hide();
					else
						$('#pagination').show();
					that.maxId = data.max_id;
					// $('#timeline').empty().html(data.timeline);
					var obj = $(data.timeline);		
					obj.find('.retweet-link a').click(that.retweet);
					obj.find('.reply a').click(that.reply);
					obj.find('.fav-action').click(that.favorites.create);
					$('#timeline').empty().append(obj);
					o.parent().removeClass('loading');
				}
			},
			error:function(data){
				o.parent().removeClass('loading');
			}
		});
	},
	getDMsg:function(o){
		var that = this;
		that.status = 'dmsg';
		clearInterval(that.intervalId);
		$.ajax({
			type:'POST',
			dataType:'json',
			url:'ajax/get_statuses.php',
			data:{'type':'twitter','status':'dmsg'},
			success:function(data){
				$('#heading').html('Direct message');
				if(data.num>0){
					if(data.num<20)	
						$('#pagination').hide();
					else
						$('#pagination').show();
					that.maxId = data.max_id;
					// $('#timeline').empty().html(data.timeline);
					var obj = $(data.timeline);		
					obj.find('.reply a').click(that.reply);
					$('#timeline').empty().append(obj);
					o.parent().removeClass('loading');
				}
			},
			error:function(data){
				o.parent().removeClass('loading');
			}
		});
	},
	getFavorites:function(o){
		var that = this;
		that.status = 'fav';
		clearInterval(that.intervalId);
		$.ajax({
			type:'POST',
			dataType:'json',
			url:'ajax/get_statuses.php',
			data:{'type':'twitter','status':'fav'},
			success:function(data){
				$('#heading').html('Your Favorites');
				if(data.num>0){
					if(data.num<20)	
						$('#pagination').hide();
					else
						$('#pagination').show();
					that.maxId = data.max_id;
					var obj = $(data.timeline);		
					obj.find('.retweet-link a').click(that.retweet);
					obj.find('.reply a').click(that.reply);
					obj.find('.fav-action').click(that.favorites.create);
					$('#timeline').empty().append(obj);
					o.parent().removeClass('loading');
				}
			},
			error:function(data){
				o.parent().removeClass('loading');
			}
		});
	},
	getStatus:function(o){
		var that = home;
		that.status = 'home_timeline';
		$.ajax({
			type:'POST',
			dataType:'json',
			url:'ajax/get_statuses.php',
			data:{'type':'twitter','status':'home_timeline'},
			success:function(data){
				that.bGetOld = true;
				that.maxId = data.max_id;
				that.page=1;
				if(data.num<20)	
					$('#pagination').hide();
				else
					$('#pagination').show();
				//$('#timeline').empty().html($(data.timeline));
				var obj = $(data.timeline);		
				obj.find('.retweet-link a').click(that.retweet);
				obj.find('.reply a').click(that.reply);
				obj.find('.fav-action').click(that.favorites.create);
				$('#timeline').empty().append(obj);
				o.parent().removeClass('loading');
			},
			error:function(data){
				
			}
		});
	},
	favorites:new Favorites()
};

//encapsulate class Favorites
function Favorites(){}
Favorites.prototype.create = function(){
	var d = eval('('+$(this).attr('data')+')');	
	data = {'api_type':'fav','id':d.id};
	var url = d.favorited?'ajax/destroy.php':'ajax/create.php';
	var that = this;
	$.ajax({
		type:'POST',
		dataType:'json',
		url:url,
		data:data,
		success:function(data){
			var str = '';
			if(data.twitter.error){
				alert('twitter error:'+data.twitter.error);
			}else{
				if(data.twitter.favorited){
					$(that).removeClass('non-fav').addClass('fav').attr('data',"{'id':"+d.id+",'favorited':1}");
				}else{
					$(that).removeClass('fav').addClass('non-fav').attr('data',"{'id':"+d.id+",'favorited':0}");
				}
			}
		},
		error:function(data){
			alert('post error'+data);
			$('#in_reply_to_status_id').val('');
		}
	});
	return false;
};

var yakexi = {
		h:home
};