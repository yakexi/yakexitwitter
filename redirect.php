<?php

/* Start session and load library. */
session_start();
require_once('config.php');
require_once('oauth/twitteroauth.php');
require_once('oauth/sinaoauth.php');
$type = 'twitter';
if($_REQUEST['type'])
	$type =$_REQUEST['type'];

if($type == 'twitter'){
	/* Build TwitterOAuth object with client credentials. */
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET); 
	/* Get temporary credentials. */
	$request_token = $connection->getRequestToken(OAUTH_CALLBACK);
	/* Save temporary credentials to session. */
	$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
	$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret']; 
	/* If last connection failed don't display authorization link. */
	switch ($connection->http_code) {
	  case 200:
	    /* Build authorize URL and redirect user to Twitter. */
	    $url = $connection->getAuthorizeURL($token);
	    //header('Location: ' . $url); 
	    echo '<a href="'.$url.'">'.$url.'</a>';
	    break;
	  default:
	    /* Show notification if something went wrong. */
	    echo 'Could not connect to Twitter. Refresh the page or try again later.';
	}
}else if($type == 'sina'){	
	$o = new WeiboOAuth( WB_AKEY , WB_SKEY);	
	$keys = $o->getRequestToken();
	$aurl = $o->getAuthorizeURL( $keys['oauth_token'] ,false , WB_CALLBACK);

	$_SESSION['keys'] = $keys;
	
	echo '<a href="'.$aurl.'">Use Oauth to login</a>';
}
