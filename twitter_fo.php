<?php
/**
 * @file
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.
 */

/* Load required lib files. */
set_time_limit(0);
require_once('config.php');
require_once('oauth/twitteroauth.php');
require_once('account/security.php');
require_once('include/class.token.php');
include_once( 'friends_keep.php' );

//Token::save('twitter',array('oauth_token'=>'key','oauth_token_secret'=>'sercet'));
/* If access tokens are not available redirect to connect page. */
if($token = Token::get('twitter')){
	$_SESSION['access_token'] = $token;
}else{
	header('Location: ./clearsessions.php');
	exit;
}
header("Content-type: text/html; charset=utf-8"); 
if($token_sina = Token::get('sina')){
	$_SESSION['access_token_sina'] = $token_sina;
	$access_token_sina = $_SESSION['access_token_sina'];
}
$username = $_SESSION['login_user']['username'];

/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
/*$rt = $connection->get('friendships/exists', array('user_a' => 'iyakexi','user_b' => 'china_finance'));
$sh = $connection->get('friendships/show', array('source_screen_name' => 'iyakexi','target_screen_name' => 'china_finance'));
echo '<pre>';
var_dump($rt);
echo '<hr/>';
var_dump($sh);
echo '</pre>';
exit;*/
$all_users = array();
$next_cursor = -1;
do{
	$friends = $connection->get('statuses/friends',array('cursor'=>$next_cursor));
	if($friends->error){
		echo '<pre>';var_dump($friends);echo '</pre>';
		exit;
	}
	$next_cursor = $friends->next_cursor;
	$users = $friends->users;
	foreach($users as $user){
		array_push($all_users,$user->screen_name);
	}
}while($next_cursor > 0);

if(count($all_users)<=0){
	echo 'error';
	echo '<pre>';var_dump($followers);print_r($all_users);echo '</pre>';
	exit;
}
echo '<pre>followers:';print_r($all_users);echo '</pre><hr/>';
$next_cursor = -1;$j=0;
do{
	$followers = $connection->get('statuses/followers',array('cursor'=>$next_cursor));
	if($followers->error){
		echo '<pre>';var_dump($followers);echo '</pre>';
		exit;
	}
	$next_cursor = $followers->next_cursor;
	$users = $followers->users;
	foreach($users as $user){
		$name = $user->screen_name;		
		if(in_array($name,$all_users)){
			echo (++$j)."|skip:{$name}<br/>";
		}else{				
			$connection->post('friendships/create', array('screen_name' => $name));	
			echo (++$j)."|<span style=\"color:green;\">fo:{$name}</span><br/>";
		}		
		ob_flush();flush();
	}
}while($next_cursor > 0);
echo $next_cursor ;
echo 'over';

/* Some example calls */
//$connection->get('users/show', array('screen_name' => 'abraham')));
//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
//$connection->post('statuses/destroy', array('id' => 5437877770));
//$connection->post('friendships/create', array('id' => 9436992)));
//$connection->post('friendships/destroy', array('id' => 9436992)));